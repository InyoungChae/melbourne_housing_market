import os
import pandas as pd
from sklearn.externals import joblib


def load_data(file_name):
    data_path = os.path.join(os.getcwd(), 'data', file_name)
    return pd.read_csv(data_path)


def save_model(model, file_name):
    model_dir = os.path.join(os.getcwd(), 'model')
    if not os.path.isdir(model_dir):
        os.mkdir(model_dir)
    model_path = os.path.join(model_dir, file_name)
    joblib.dump(model, model_path)


def load_model(file_name):
    model_dir = os.path.join(os.getcwd(), 'model')
    model_path = os.path.join(model_dir, file_name)
    return joblib.load(model_path)


def extract_data_info_from_dataframe(df):
    data_info = dict()
    data_info['names'] = df.dtypes.index.tolist()
    data_info['dtypes'] = df.dtypes.tolist()
    return data_info


# ndarray에 data_info가 있으면 dataframe을 만들 수 있음
def convert_ndarray_to_dataframe(X, data_info=None):
    from pandas import DataFrame

    X = DataFrame(X)

    if data_info is None:
        return X
    
    X.columns = data_info['names']
    dtypes_dict = dict(zip(data_info['names'], data_info['dtypes']))
    X = X.astype(dtypes_dict)
    return X