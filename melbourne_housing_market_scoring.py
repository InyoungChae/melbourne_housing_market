# 1. Data Prepare
def prepare_data():
    from util.io_functions import load_data, extract_data_info_from_dataframe
    housing = load_data("Melbourne_housing_scoringset.csv")
    response = 'Price'

    df_X = housing.drop(['Unnamed: 0', response], axis=1)
    data_info = extract_data_info_from_dataframe(df_X)
    data_info['string_columns'] = ['Suburb', 'Address', 'SellerG', 'Date', 'YearBuilt', 'CouncilArea', 'Regionname']
    data_info['categorical_columns'] = ['Type', 'Method']
    return df_X, data_info


# 2. Scorer #사용자 정의 트랜스포머의 정의가 함께 있어야 unpickle 가능
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import LabelEncoder


class RemoveStringColumns(BaseEstimator, TransformerMixin):
    def __init__(self, data_info):
        self.data_info = data_info

    def fit(self, x, y=None):
        return self

    def transform(self, x, y=None):
        x = x.drop(self.data_info['string_columns'], axis=1)
        print('Remove String Columns Completed')
        return x


class MultiColumnLabelEncoder(BaseEstimator, TransformerMixin):
    def __init__(self, data_info):
        self.data_info = data_info  # array of column names to encode

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        '''
        Transforms columns of X specified in self.columns using
        LabelEncoder().
        '''
        for col in self.data_info['categorical_columns']:
            X[col] = LabelEncoder().fit_transform(X[col])
            print('LabelEncodel Completed')
        return X


def score(df_X, data_info):
    import os
    from sklearn.externals import joblib

    model_dir = os.path.join(os.getcwd(), 'model')
    model_path = os.path.join(model_dir, 'ridge_pipeline.pkl')
    model = joblib.load(model_path)
    print(model.steps)

    predicted_price = model.predict(df_X)
    print('predicted_price: ', predicted_price)
    return predicted_price


def run_scoring():
    df_X, data_info = prepare_data()
    predicted_price = score(df_X, data_info)


if __name__ == '__main__':
    run_scoring()