#global variable
random_state = 42


# 1. data prepare
def prepare_data():
    from util.io_functions import load_data, extract_data_info_from_dataframe
    import numpy as np
    housing = load_data("Melbourne_housing_FULL.csv")
    response = 'Price'

    housing_wdout_price = housing[np.isnan(housing["Price"])]  # scoringset(without price)
    housing_wdout_price.to_csv('./data/Melbourne_housing_scoringset.csv')
    housing_wd_price = housing[~np.isnan(housing["Price"])]  # trainingset(with price)

    df_X = housing_wd_price.drop(response, axis=1)
    df_y = housing_wd_price[response].copy()
    data_info = extract_data_info_from_dataframe(df_X)
    data_info['string_columns'] = ['Suburb', 'Address', 'SellerG', 'Date', 'YearBuilt', 'CouncilArea', 'Regionname']
    data_info['categorical_columns'] = ['Type', 'Method']
    return df_X, df_y, data_info


# 2. modeling pipeline
# user-defined transformer
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import LabelEncoder


class RemoveStringColumns(BaseEstimator, TransformerMixin):
    def __init__(self, data_info):
        self.data_info = data_info

    def fit(self, x, y=None):
        return self

    def transform(self, x, y=None):
        x = x.drop(self.data_info['string_columns'], axis=1)
        print('Remove String Columns Completed')
        return x


class MultiColumnLabelEncoder(BaseEstimator, TransformerMixin):
    def __init__(self, data_info):
        self.data_info = data_info  # array of column names to encode

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        '''
        Transforms columns of X specified in self.columns using
        LabelEncoder().
        '''
        for col in self.data_info['categorical_columns']:
            X[col] = LabelEncoder().fit_transform(X[col])
            print('LabelEncodel Completed')
        return X


# modeling
def modeling_pipeline(df_X, df_y, data_info):
    import numpy as np
    from sklearn.pipeline import Pipeline
    from sklearn.model_selection import train_test_split
    from sklearn.preprocessing import Imputer
    from sklearn.model_selection import RandomizedSearchCV
    from scipy.stats import randint
    from sklearn.metrics import mean_squared_error
    from sklearn.linear_model import Ridge
    from util.io_functions import save_model, convert_ndarray_to_dataframe
    import matplotlib.pyplot as plt

    X_train, X_test, y_train, y_test = train_test_split(df_X, df_y, test_size=0.2, random_state=random_state)

    pipeline = Pipeline([
        ('Remover', RemoveStringColumns(data_info)),
        ('LabelEncoder', MultiColumnLabelEncoder(data_info)),
        ('Imputer', Imputer(strategy='median')),
        ('Regressor', Ridge(alpha=0.5, random_state=random_state))
    ])

    parameters_distribs = {
        'Imputer__strategy': ['mean', 'median', 'most_frequent'],
        'Regressor__alpha': randint(low=0.1, high=10)
    }

    rnd_search = RandomizedSearchCV(pipeline, param_distributions=parameters_distribs,
                                    n_iter=10, cv=3, scoring='neg_mean_squared_error')

    rnd_search.fit(X_train, y_train)
    best_model = rnd_search.best_estimator_
    coefficients = best_model.named_steps['Regressor'].intercept_ + best_model.named_steps['Regressor'].coef_
    y_pred = best_model.predict(X_test)
    final_mse = mean_squared_error(y_test, y_pred)
    final_rmse = np.sqrt(final_mse)
    print('rmse: ', final_rmse)

    df_total_X = np.r_[X_train, X_test]
    df_total_y = np.r_[y_train, y_test]
    df_total_X = convert_ndarray_to_dataframe(df_total_X, data_info)

    best_model.fit(df_total_X, df_total_y)

    predictors = [col for col in data_info['names'] if col not in data_info['string_columns']]
    model_info = {
        'model': {
            'pipeline': best_model.steps
        },
        'metric': {
            'rmse': final_rmse
        },
        'interpretation': {
            'coefficients': list(zip(['intercept'] + predictors, coefficients))
        }
    }

    #     # plot most important features
    #     feat_imp = pd.Series(coefficients, index=predictors).sort_values(ascending=False)
    #     feat_imp[:40].plot(kind='bar', title='coefficients', figsize=(12, 8))
    #     plt.ylabel('coefficients')
    #     plt.subplots_adjust(bottom=0.3)
    #     plt.savefig('coefficients.png')
    #     plt.show()

    save_model(best_model, 'ridge_pipeline.pkl')
    print(model_info)

    return model_info


def run_training():
    df_X, df_y, data_info = prepare_data()
    model_info = modeling_pipeline(df_X, df_y, data_info)


if __name__ == '__main__':
    run_training()